Minecraft 1.16 mod for the [Fabric mod loader](https://www.fabricmc.net/d). 

This is an incomplete mod. 

Features can be configured either through [Mod Menu](https://www.curseforge.com/minecraft/mc-mods/modmenu/files/all?filter-game-version=1738749986%3a70886) or by editing `.minecraft/config/mod_id.json`

If you'd like to contribute a translation, follow [this link](https://crowdin.com/project/mod-id). 
Translations will be available once approved without the need to update the mod thanks to [CrowdinTranslate](https://github.com/gbl/CrowdinTranslate).

[![Requires the Fabric API](https://i.imgur.com/Ol1Tcf8.png)](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all?filter-game-version=1738749986%3a70886)

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.