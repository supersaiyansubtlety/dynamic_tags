package net.sssubtlety.dynamic_tags;

import com.google.common.collect.ImmutableList;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;

import java.util.*;

public abstract class IdentifiedTag<T> implements Tag.Identified<T> {
    private final Identifier id;
    public final ImmutableList<T> valueList;

    public IdentifiedTag(Identifier id, List<T> valuesList) {
        this.id = id;
        this.valueList = ImmutableList.copyOf(valuesList);
    }

    @Override
    final public Identifier getId() {
        return id;
    }

    @Override
    final public List<T> values() {
        return valueList;
    }

    @Override
    final public T getRandom(Random random) {
        return valueList.get(MathHelper.nextInt(random, 0, valueList.size()));
    }

    @Override
    public abstract boolean contains(Object element);
}
