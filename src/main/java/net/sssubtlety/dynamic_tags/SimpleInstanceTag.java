package net.sssubtlety.dynamic_tags;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

import java.util.List;

public class SimpleInstanceTag<T, C extends T> extends DynamicInstanceTag<T, C> {

    public SimpleInstanceTag(Identifier id, List<T> valuesList, Class<C> clazz) {
        super(id, valuesList, clazz);
    }

    @Override
    protected Object convertT(Object o) {
        return o;
    }

    public static <T, C extends T> List<T> makeValuesList(Iterable<T> superList, Class<C> clazz) {
        return DynamicTagsApi.makeValuesList(superList, clazz::isInstance);
    }

    public static Tag.Identified<Block> forBlock(Class<? extends Block> blockClass, Identifier id, List<Block> matchList) {
        Tag.Identified<Block> tag = new SimpleInstanceTag<>(id, matchList, blockClass);
        DynamicTags.blockTagBuilder.put(tag.getId(), tag);
        return tag;
    }

    public static Tag.Identified<Block> forBlockMakeList(Class<? extends Block> blockClass, Identifier id, Iterable<Block> superList) {
        return forBlock(blockClass, id, makeValuesList(superList, blockClass));
    }

    public static Tag.Identified<Item> forItem(Class<? extends Item> itemClass, Identifier id, List<Item> matchList) {
        Tag.Identified<Item> tag = new SimpleInstanceTag<>(id, matchList, itemClass);
        DynamicTags.itemTagBuilder.put(tag.getId(), tag);
        return tag;
    }

    public static Tag.Identified<Item> forItemMakeList(Class<? extends Item> itemClass, Identifier id, Iterable<Item> superList) {
        return forItem(itemClass, id, makeValuesList(superList, itemClass));
    }


    public static Tag.Identified<Fluid> forFluid(Class<? extends Fluid> fluidClass, Identifier id, List<Fluid> matchList) {
        Tag.Identified<Fluid> tag = new SimpleInstanceTag<>(id, matchList, fluidClass);
        DynamicTags.fluidTagBuilder.put(tag.getId(), tag);
        return tag;
    }

    public static Tag.Identified<Fluid> forFluidMakeList(Class<? extends Fluid> fluidClass, Identifier id, Iterable<Fluid> superList) {
        return forFluid(fluidClass, id, makeValuesList(superList, fluidClass));
    }


    public static Tag.Identified<EntityType<?>> forEntityType(Class<? extends EntityType<?>> entityTypeClass, Identifier id, List<EntityType<?>> matchList) {
        Tag.Identified<EntityType<?>> tag = new SimpleInstanceTag<>(id, matchList, entityTypeClass);
        DynamicTags.entityTypeTagBuilder.put(tag.getId(), tag);
        return tag;
    }

    public static Tag.Identified<EntityType<?>> forEntityTypeMakeList(Class<? extends EntityType<?>> entityTypeClass, Identifier id, Iterable<EntityType<?>> superList) {
        return forEntityType(entityTypeClass, id, makeValuesList(superList, entityTypeClass));
    }
}
