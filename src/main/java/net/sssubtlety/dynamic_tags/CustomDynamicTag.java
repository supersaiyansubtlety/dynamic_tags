package net.sssubtlety.dynamic_tags;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

import java.util.*;
import java.util.function.Predicate;

import static net.sssubtlety.dynamic_tags.DynamicTagsApi.blockToItemList;
import static net.sssubtlety.dynamic_tags.DynamicTagsApi.makeValuesList;

public class CustomDynamicTag<T> extends NonAirTag<T> {
    protected final Predicate<T> matcher;

    public CustomDynamicTag(Identifier id, List<T> valuesList, Predicate<T> matcher) {
        super(id, valuesList);
        this.matcher = matcher;
    }

    public static void createForBlock(Predicate<Block> blockMatcher, Identifier id, List<Block> matchList) {
        Tag.Identified<Block> blockTag = new CustomDynamicTag<>(id, matchList, blockMatcher);
        DynamicTags.blockTagBuilder.put(blockTag.getId(), blockTag);
    }

    public static void createForItem(Identifier id, Predicate<Item> itemMatcher, List<Item> matchList) {
        Tag.Identified<Item> itemTag = new CustomDynamicTag<>(id, matchList, itemMatcher);
        DynamicTags.itemTagBuilder.put(itemTag.getId(), itemTag);
    }

    public static void createForBlockItem(Predicate<Item> itemMatcher, Identifier id, List<Block> matchList) {
        createForItem(id, itemMatcher, blockToItemList(matchList));
    }

    public static <T> CustomDynamicTag<T> fromSuperList(Identifier id, Iterable<T> superList, Predicate<T> matcher) {
        return new CustomDynamicTag<>(id, makeValuesList(superList, matcher), matcher);
    }

    @Override
    public boolean contains(Object entry) {
        if (!super.contains(entry)) return false;
        try {
            return matcher.test((T) entry);
        } catch (ClassCastException e) {
            return false;
        }
    }

}
