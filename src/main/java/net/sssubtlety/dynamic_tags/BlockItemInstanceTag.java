package net.sssubtlety.dynamic_tags;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

import java.util.List;

import static net.sssubtlety.dynamic_tags.DynamicTagsApi.blockToItemList;

public class BlockItemInstanceTag<B extends Block> extends DynamicInstanceTag<Item, B> {

    public BlockItemInstanceTag(Identifier id, List<Item> valuesList, Class<B> blockClass) {
        super(id, valuesList, blockClass);
    }

    public static void registerFor(Class<? extends Block> blockClass, Identifier id, List<Block> matchList) {
        Tag.Identified<Item> itemTag = new BlockItemInstanceTag<>(id, blockToItemList(matchList), blockClass);
        DynamicTags.itemTagBuilder.put(itemTag.getId(), itemTag);
    }

    public static <B extends Block> BlockItemInstanceTag<B> fromSuperList(Identifier id, Iterable<Item> superList, Class<B> blockClass) {
        return new BlockItemInstanceTag<>(id, makeValuesList(superList,blockClass), blockClass);
    }

    public static <B extends Block> List<Item> makeValuesList(Iterable<Item> superList, Class<B> blockClass) {
        return DynamicTagsApi.makeValuesList(superList, item -> item instanceof BlockItem && blockClass.isInstance(((BlockItem)item).getBlock()));
    }

    @Override
    protected Object convertT(Object item) {
        return item instanceof BlockItem ? ((BlockItem)item).getBlock() : null;
    }
}
