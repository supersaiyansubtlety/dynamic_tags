package net.sssubtlety.dynamic_tags;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.Blocks;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;

import java.util.List;

public class NonAirTag<T> extends IdentifiedTag<T>{
    public static final ImmutableSet<Object> ALL_AIR = (new ImmutableSet.Builder<>().add(
            Blocks.AIR,
            Items.AIR,
            Blocks.VOID_AIR,
            Blocks.CAVE_AIR
    ).build());
    //{
//        Blocks.AIR,
//        Items.AIR,
//        Blocks.VOID_AIR,
//        Blocks.CAVE_AIR
//    };
    public NonAirTag(Identifier id, List<T> valuesList) {
        super(id, DynamicTagsApi.newListWithoutAny(valuesList, ALL_AIR));
    }

    @Override
    public boolean contains(Object element) {
        return !ALL_AIR.contains(element);
    }
}
