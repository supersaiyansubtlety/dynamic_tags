package net.sssubtlety.dynamic_tags;

import net.minecraft.util.Identifier;

import java.util.List;

public abstract class DynamicInstanceTag<T, C> extends NonAirTag<T> {
    protected final Class<C> clazz;

    public DynamicInstanceTag(Identifier id, List<T> valuesList, Class<C> clazz) {
        super(id, valuesList);
        this.clazz = clazz;
    }

    protected abstract Object convertT(Object t);

    @Override
    public boolean contains(Object entry) {
        return super.contains(entry) && clazz.isInstance(convertT(entry));
    }
}
