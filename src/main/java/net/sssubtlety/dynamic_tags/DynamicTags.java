package net.sssubtlety.dynamic_tags;

import com.google.common.collect.ImmutableMap;
import net.minecraft.block.*;
import net.minecraft.entity.EntityType;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.sssubtlety.dynamic_tags.DynamicTagsApi;
import net.sssubtlety.dynamic_tags.mixin.AbstractBlockAccessor;
import net.sssubtlety.dynamic_tags.mixin.AbstractBlockStateAccessor;

import java.util.List;

import static net.sssubtlety.dynamic_tags.DynamicTagsApi.*;

public abstract class DynamicTags {
    private static ImmutableMap<Identifier, Tag<Block>> BLOCK_TAGS;
    private static ImmutableMap<Identifier, Tag<Item>> ITEM_TAGS;
    private static ImmutableMap<Identifier, Tag<Fluid>> FLUID_TAGS;
    private static ImmutableMap<Identifier, Tag<EntityType<?>>> ENTITY_TYPE_TAGS;

    public static ImmutableMap<Identifier, Tag<Block>> getBlockTags() {
        return BLOCK_TAGS;
    }
    public static ImmutableMap<Identifier, Tag<Item>> getItemTags() {
        return ITEM_TAGS;
    }
    public static ImmutableMap<Identifier, Tag<Fluid>> getFluidTags() {
        return FLUID_TAGS;
    }
    public static ImmutableMap<Identifier, Tag<EntityType<?>>> getEntityTypeTags() {
        return ENTITY_TYPE_TAGS;
    }

    static final ImmutableMap.Builder<Identifier, Tag<Block>> blockTagBuilder = new ImmutableMap.Builder<>();
    static final ImmutableMap.Builder<Identifier, Tag<Item>> itemTagBuilder = new ImmutableMap.Builder<>();
    static final ImmutableMap.Builder<Identifier, Tag<Fluid>> fluidTagBuilder = new ImmutableMap.Builder<>();
    static final ImmutableMap.Builder<Identifier, Tag<EntityType<?>>> entityTypeTagBuilder = new ImmutableMap.Builder<>();

    static void init() {
        // custom block/item tags
        createBlockAndItemCustomTag(makeId("full"), DynamicTagsApi::isFullCube, Registry.BLOCK);
        createBlockAndItemCustomTag(makeId("partial"), block -> !isFullCube(block), Registry.BLOCK);

        createBlockAndItemCustomTag(makeId("solid"), block -> ((AbstractBlockAccessor)block).getMaterial().isSolid(), Registry.BLOCK);
        createBlockAndItemCustomTag(makeId("non_solid"), block -> !((AbstractBlockAccessor)block).getMaterial().isSolid(), Registry.BLOCK);

        createBlockAndItemCustomTag(makeId("opaque"), block -> ((AbstractBlockStateAccessor)block.getDefaultState()).isOpaque(), Registry.BLOCK);
        createBlockAndItemCustomTag(makeId("transparent"), block -> !((AbstractBlockStateAccessor)block.getDefaultState()).isOpaque(), Registry.BLOCK);

        createBlockAndItemCustomTag(makeId("collidable"), block -> ((AbstractBlockAccessor)block).isCollidable(), Registry.BLOCK);
        createBlockAndItemCustomTag(makeId("intangible"), block -> !((AbstractBlockAccessor)block).isCollidable(), Registry.BLOCK);

        createBlockAndItemCustomTag(makeId("emits_redstone_power"), block -> block.getDefaultState().emitsRedstonePower(), Registry.BLOCK);

        // instance block/item tags

        List<Block> blockEntityProvider =
        createBlockAndItemInterfaceInstanceTag(makeId("block_entity_provider"), BlockEntityProvider.class, Registry.BLOCK);
        List<Block> waterloggableBlocks =
        createBlockAndItemInterfaceInstanceTag(makeId("waterloggable"), Waterloggable.class, Registry.BLOCK);

        createBlockAndItemInstanceTag(makeId("banner"), AbstractBannerBlock.class, blockEntityProvider);
        createBlockAndItemInstanceTag(makeId("rail"), AbstractRailBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("pressure_plate"), AbstractPressurePlateBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("button"), AbstractButtonBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("furnace"), AbstractFurnaceBlock.class, blockEntityProvider);
        createBlockAndItemInstanceTag(makeId("fire"), AbstractFireBlock.class, Registry.BLOCK);
        createBlockAndItemInterfaceInstanceTag(makeId("inventory_provider"), InventoryProvider.class, Registry.BLOCK);

        createBlockAndItemInstanceTag(makeId("spreadable"), SpreadableBlock.class, Registry.BLOCK);
        createBlockAndItemInterfaceInstanceTag(makeId("fertilizable"), Fertilizable.class, Registry.BLOCK);
        createBlockAndItemInterfaceInstanceTag(makeId("stainable"), Stainable.class, Registry.BLOCK);

        createBlockAndItemInstanceTag(makeId("stairs"), StairsBlock.class, waterloggableBlocks);
        createBlockAndItemInstanceTag(makeId("slab"), SlabBlock.class, waterloggableBlocks);

        List<Block> plantBlocks =
        createBlockAndItemInstanceTag(makeId("plant"), PlantBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("tall_plant"), TallPlantBlock.class, plantBlocks);
        createBlockAndItemInstanceTag(makeId("crop"), CropBlock.class, plantBlocks);
        createBlockAndItemInstanceTag(makeId("sapling"), SaplingBlock.class, plantBlocks);
        List<Block> flowerBlocks =
        createBlockAndItemCustomTag(makeId("flower"), block -> block instanceof FlowerBlock || block instanceof TallFlowerBlock, plantBlocks);
        createBlockAndItemInstanceTag(makeId("small_flower"), FlowerBlock.class, flowerBlocks);
        createBlockAndItemInstanceTag(makeId("tall_flower"), TallFlowerBlock.class, flowerBlocks);

        createBlockAndItemInstanceTag(makeId("flower_pot"), FlowerPotBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("leaves"), LeavesBlock.class, Registry.BLOCK);

        createBlockAndItemInstanceTag(makeId("fluid"), FluidBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("pillar"), PillarBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("bed"), BedBlock.class, blockEntityProvider);
        createBlockAndItemInstanceTag(makeId("infested"), InfestedBlock.class, blockEntityProvider);
        createBlockAndItemInstanceTag(makeId("ore"), OreBlock.class, blockEntityProvider);
        createBlockAndItemInstanceTag(makeId("sign"), SignBlock.class, blockEntityProvider);
        createBlockAndItemInstanceTag(makeId("torch"), TorchBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("door"), DoorBlock.class, Registry.BLOCK);
        createBlockAndItemInstanceTag(makeId("trapdoor"), TrapdoorBlock.class, waterloggableBlocks);

        createBlockAndItemInstanceTag(makeId("fence"), FenceBlock.class, waterloggableBlocks);
        createBlockAndItemInstanceTag(makeId("fence_gate"), FenceGateBlock.class, waterloggableBlocks);
        createBlockAndItemInstanceTag(makeId("wall"), WallBlock.class, waterloggableBlocks);
        createBlockAndItemInstanceTag(makeId("pane"), PaneBlock.class, waterloggableBlocks);

        DynamicTagsApi.initialize();

        BLOCK_TAGS = blockTagBuilder.build();
        ITEM_TAGS = itemTagBuilder.build();
        FLUID_TAGS = fluidTagBuilder.build();
        ENTITY_TYPE_TAGS = entityTypeTagBuilder.build();
    }

    private static Identifier makeId(String path) {
        return new Identifier(MOD_ID, path);
    }
}
