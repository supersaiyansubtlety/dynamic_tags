package net.sssubtlety.dynamic_tags;

import net.fabricmc.api.ModInitializer;

public class DynamicTagsInit implements ModInitializer {
	@Override
	public void onInitialize() {
		DynamicTags.init();
	}
}
