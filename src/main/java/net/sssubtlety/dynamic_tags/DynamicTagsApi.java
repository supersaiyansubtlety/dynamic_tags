package net.sssubtlety.dynamic_tags;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.sssubtlety.dynamic_tags.mixin.AbstractBlockStateAccessor;
import net.sssubtlety.dynamic_tags.mixin.ShapeCacheAccessor;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public interface DynamicTagsApi {
    String MOD_ID = "dynamic_tags";

    void init();

    static void initialize() {
        FabricLoader.getInstance().getEntrypointContainers(MOD_ID, DynamicTagsApi.class)
                .forEach(entryPoint -> ((DynamicTagsApi)entryPoint).init());
    }

    static List<Block> createBlockAndItemInstanceTag(Identifier id, Class<? extends Block> blockClass, Iterable<Block> superList) {
        List<Block> matchList = SimpleInstanceTag.makeValuesList(superList, blockClass);

        SimpleInstanceTag.forBlock(blockClass, id, matchList);
        BlockItemInstanceTag.registerFor(blockClass, id, matchList);

        return matchList;
    }

    static <I> List<Block> createBlockAndItemInterfaceInstanceTag(Identifier id, Class<I> iface, Iterable<Block> superList) {
        if (!iface.isInterface()) throw new IllegalArgumentException("createBlockAndItemInterfaceInstanceTag passed non-Interface class: " + iface.toString() + " for id '" + id.toString() + "'. ");
        Predicate<Block> blockMatcher = iface::isInstance;
        Predicate<Item> itemMatcher = item -> item instanceof BlockItem && blockMatcher.test(((BlockItem)item).getBlock());
        List<Block> matchList = makeValuesList(superList, blockMatcher);

        CustomDynamicTag.createForBlock(blockMatcher, id, matchList);
        CustomDynamicTag.createForBlockItem(itemMatcher, id, matchList);

        return matchList;
    }

    static List<Block> createBlockAndItemCustomTag(Identifier id, Predicate<Block> blockPredicate, Iterable<Block> superList) {
        List<Block> matchList = makeValuesList(superList, blockPredicate);

        CustomDynamicTag.createForBlock(blockPredicate, id, matchList);

        CustomDynamicTag.createForBlockItem(item -> item instanceof BlockItem && blockPredicate.test(((BlockItem)item).getBlock()), id, matchList);

        return matchList;
    }

    static List<Item> blockToItemList(Iterable<Block> blocks) {
        return Lists.newArrayList(Iterables.transform(blocks, Block::asItem));
    }

    static <T> List<T> newListWithoutAny(Iterable<T> sourceList, ImmutableSet<Object> elements) {
        List<T> newList = Lists.newArrayList(sourceList);
        int i = 0;
        while (i < newList.size()) {
            if (elements.contains(newList.get(i)))
                newList.remove(i);
            else
                i++;
        }

        return newList;
    }

    static <T> List<T> makeValuesList(Iterable<T> superList, Predicate<T> matcher) {
        List<T> valuesList = new LinkedList<>();
        superList.forEach(t -> { if (matcher.test(t)) valuesList.add(t); });
        return valuesList;
    }

    static boolean isFullCube(Block block) {
        return getShapeCache(block) != null && ((ShapeCacheAccessor)(Object) getShapeCache(block)).getIsFullCube();
    }

    static AbstractBlock.AbstractBlockState.ShapeCache getShapeCache(Block block) {
        BlockState state = block.getDefaultState();
        state.initShapeCache();
        return ((AbstractBlockStateAccessor)state).getShapeCache();
    }

    static boolean equalsAny(Object o, Iterable<Object> others) {
        for (Object other : others)
            if (o.equals(other)) return true;

        return false;
    }
}
