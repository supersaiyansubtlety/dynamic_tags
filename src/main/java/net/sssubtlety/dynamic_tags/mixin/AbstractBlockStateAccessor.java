package net.sssubtlety.dynamic_tags.mixin;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Material;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(AbstractBlock.AbstractBlockState.class)
public interface AbstractBlockStateAccessor {
    @Accessor
    AbstractBlock.AbstractBlockState.ShapeCache getShapeCache();

    @Accessor boolean isOpaque();
}
