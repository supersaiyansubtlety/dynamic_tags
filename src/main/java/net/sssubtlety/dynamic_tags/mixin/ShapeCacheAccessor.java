package net.sssubtlety.dynamic_tags.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(targets = "net.minecraft.block.AbstractBlock$AbstractBlockState$ShapeCache")
public interface ShapeCacheAccessor {
    @Accessor("isFullCube") boolean getIsFullCube();
}
