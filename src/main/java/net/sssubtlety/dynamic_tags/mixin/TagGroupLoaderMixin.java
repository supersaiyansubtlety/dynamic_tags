package net.sssubtlety.dynamic_tags.mixin;

import net.minecraft.tag.Tag;
import net.minecraft.tag.TagGroupLoader;
import net.minecraft.util.Identifier;
import net.sssubtlety.dynamic_tags.DynamicTags;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

import java.util.Map;

@Mixin(TagGroupLoader.class)
public class TagGroupLoaderMixin <T> {
	@Shadow @Final private String dataType;

	@ModifyArg(
		method = "buildGroup", index = 0, at = @At(value = "INVOKE",
			target = "Lnet/minecraft/tag/TagGroup;create(Ljava/util/Map;)Lnet/minecraft/tag/TagGroup;"
		)
	)
	private Map<Identifier, Tag<?>> postApplyReload(Map<Identifier, Tag<?>> map) {
		final String typeString = dataType.substring(5);
		switch (typeString) {
			case "blocks" -> map.putAll(DynamicTags.getBlockTags());
			case "items" -> map.putAll(DynamicTags.getItemTags());
//			case "fluid" ->
//			case "entity_type" ->
		}

		return map;
	}
}
