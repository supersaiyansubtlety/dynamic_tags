package net.sssubtlety.dynamic_tags.mixin;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Material;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(AbstractBlock.class)
public interface AbstractBlockAccessor {
    @Accessor Material getMaterial();

    @Accessor boolean isCollidable();

    @Accessor float getResistance();

    @Accessor boolean isRandomTicks();

    @Accessor float getSlipperiness();

    @Accessor AbstractBlock.Settings getSettings();
}
