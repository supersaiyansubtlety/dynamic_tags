Minecraft 1.16 mod for the [Fabric ModLoader](https://www.fabricmc.net/). 

Dynamic Tags creates tags that, rather than checking static lists defined by JSON files, 
instead run checks at runtime to determine matches. 

Some illustrative examples are:
- "dynamic_tags:stairs" matches any stair block. Mod-added stairs are matched without other mod creators having to do anything. 
- "dynamic_tags:opaque" matches any opaque block. This is any block that completely blocks light from passing through 
    (e.g. concrete, not slabs and stairs). Again, modded blocks are included automatically. 
    
All block tags provided by this mod are also available as item tags that match the item form of their respective blocks. 

Mod creators can also create their own dynamic tags. 

More information for both users and developers is available on the wiki. 

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
